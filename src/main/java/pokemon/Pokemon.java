package pokemon;

import java.util.Random;

public class Pokemon {
    ////// Oppgave 1a
    // Create field variables here:
    String name;
    int strength;
    int MaxHP;
    int CurrentHP;

    ///// Oppgave 1b
    public Pokemon() {
        this.name = "Zubat";
        this.strength = 1;
        this.MaxHP = 33;
        this.CurrentHP = this.MaxHP;
    }

    public Pokemon(String name, int MaxHP, int strength) {
        this.name = name;
        this.strength = strength;
        this.MaxHP = MaxHP;
        this.CurrentHP = MaxHP;
    }


    ///// Oppgave 2

    /**
     * Get name of the pokémon
     *
     * @return name of pokémon
     */
    String getName() {
        return this.name;
    }

    /**
     * Get strength of the pokémon
     *
     * @return strength of pokémon
     */
    int getStrength() {
        return this.strength;
    }

    /**
     * Get current health points of pokémon
     *
     * @return current HP of pokémon
     */
    int getCurrentHP() {
        return this.CurrentHP;
    }

    /**
     * Get maximum health points of pokémon
     *
     * @return max HP of pokémon
     */
    int getMaxHP() {
        return this.MaxHP;
    }

    /**
     * Check if the pokémon is alive.
     * A pokemon is alive if current HP is higher than 0
     *
     * @return true if current HP > 0, false if not
     */
    boolean isAlive() {
        return (CurrentHP>0);
    }


    ///// Oppgave 4

    /**
     * The Pokémon takes a given amount of damage. This method reduces the number of
     * health points the pokémon has by <code>damageTaken</code>.
     * If <code>damageTaken</code> is higher than the number of current
     * health points then set current HP to 0.
     * <p>
     * It should not be possible to deal negative damage, i.e. increase the number of health points.
     * <p>
     * The method should print how much HP the Pokémon is left with.
     *
     * @param damageTaken the amount to reduce the Pokémon's HP by
     */
    void takeDamage(int damageTaken) {
        this.CurrentHP = max(this.CurrentHP - max(damageTaken, 1), 0);

        System.out.printf("%s takes %d damage and is left with %d/%d HP\n", 
                          this.getName(), damageTaken, this.getCurrentHP(), this.getMaxHP());
    }










    ///// Oppgave 5
        
    /**
     * Attack another pokémon. The method conducts an attack by <code>this</code>
     * on <code>target</code>. Calculate the damage using the pokémons strength
     * and a random element. Reduce <code>target</code>s health.
     * <p>
     * If <code>target</code> has 0 HP then print that it was defeated.
     *
     * @param target pokémon that is being attacked
     */
    void attack(Pokemon target) {
        Random rand = new Random();
        int damageInflincted = (int) (rand.nextInt(this.strength+1));
        
        System.out.printf("%s attacks %s\n", this.getName(), target.getName());
        target.takeDamage(damageInflincted);

        if (!target.isAlive()) {
            System.out.printf("%s is defeated by %s\n", target.getName(), this.getName());
        }
    }

    ///// Oppgave 3
    @Override
    public String toString() {
        String text = this.getName() + " HP: (" 
                    + this.getCurrentHP() + "/" 
                    + this.getMaxHP() + ") STR: " 
                    + this.getStrength();
        return text;
    }






    ///// Egne Hjelpemetoder!
    private int max(int a, int b) {
        if(a>=b) return a;
        else return b;
    }
}
